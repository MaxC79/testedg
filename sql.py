import psycopg2, psycopg2.extras


def insert_dados(dados):
    connection = psycopg2.connect(
        user="postgres", 
        password="admin", 
        host="127.0.0.1", 
        port="5432", 
        database="testedg")
    cursor = connection.cursor()

    try:
        insert = """ INSERT INTO usuarios (nome, data_nascimento) VALUES (%s, %s)"""
        dados_to_insert = (dados[0], dados[1])
        cursor.execute(insert, dados_to_insert)
        connection.commit()
        return True

    except (Exception, psycopg2.Error) as error :
        if(connection):
            print("Erro na função insert_dados:", error)

    finally:
        if(connection):
            cursor.close()
            connection.close()


def select_dados():
    connection = psycopg2.connect(
        user="postgres", 
        password="admin", 
        host="127.0.0.1", 
        port="5432", 
        database="testedg")
    cursor = connection.cursor(cursor_factory=psycopg2.extras.DictCursor)

    try:
        select = """SELECT nome, date_part('year', age(data_nascimento)) as idade FROM usuarios"""
        cursor.execute(select)
        resultado = cursor.fetchall()
        return resultado

    except (Exception, psycopg2.Error) as error :
        if(connection):
            print("Erro na função select_dados:", error)
    finally:
        if(connection):
            cursor.close()
            connection.close()