from flask import Flask
from flask import request, render_template, redirect,url_for, flash
from sql import insert_dados, select_dados


app = Flask(__name__)
app.config['SECRET_KEY'] = 'UilMGSdixHuLMoHVtUk1ng'
title = 'Teste DG'


@app.route('/')
def index():
    return render_template('inicio.html', title = title)


@app.route('/dados', methods=['GET', 'POST'])
def dados():
    if (request.method == 'POST'):
        dados = [request.form['i_nome'], request.form['i_data_nascimento']]
        if insert_dados(dados):
            messagem = 'Os dados foram salvos com sucesso!'
            flash(messagem)
        return render_template('inicio.html', title = title)
    else:
        redirect(url_for('index'))


@app.route('/usuarios')
def usuarios():
    resultado = select_dados()
    return render_template('usuarios.html', title = title, resultado = resultado)


if __name__ == '__main__':
    app.run()